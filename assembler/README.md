# Assembler
Usage:
```sh
python3 ./assembler.py [input file path]  [output file path]
```

Converts assembly code to machine code for the CPU.

Example:
```sh
python3 ./assembler.py ./code.asm  ./code.bin
```
