# Instruction Set Architecture
* 64 bit instruction size
* 16384 general registers (16 bits signed)
* 2 loop counters (14 bits unsigned)
* 16 bit data access alignment
* 5 bits for opcode (32 different instructions)
* 14 bit address for register file


# REGISTERS
16384 general registers (16 bits signed) denoted by REGISTER_FILE[ADDR]. 2 loop counters (14 bits unsigned) denoted by LOOP_COUNT0 and LOOP_COUNT1. Multiple read/writes ports for register file. 16 bit single port RAM is denoted by RAM.


# List of Instructions by Opcode
0. [LOAD0](./instructions.md#load0)
1. [LOAD1](./instructions.md#load1)
2. [STORE0](./instructions.md#store0)
3. [STORE1](./instructions.md#store1)
4. [WHILE](./instructions.md#while)
5. [FOR](./instructions.md#for)
6. [ADD0](./instructions.md#add0)
7. [ADD1](./instructions.md#add1)
8. [SUB0](./instructions.md#sub0)
9. [SUB1](./instructions.md#sub1)
10. [DOT0](./instructions.md#dot0)
11. [DOT1](./instructions.md#dot1)
12. [SUM0](./instructions.md#sum0)
13. [SUM1](./instructions.md#sum1)



# LOAD0
LOAD0 instruction loades a vector from RAM to the register file. Uses LOOP_COUNT0.

```c
LOAD0 REG_DEST_ADDR, SOURCE_ADDR, VECTOR_SIZE
```
INSTRUCTION<63..59> = OPCODE

INSTRUCTION<58..45> = REG_DEST_ADDR

INSTRUCTION<44..14> = SOURCE_ADDR

INSTRUCTION<13..0> = VECTOR_SIZE

Pseudo code
```c
unsigned int i;
for (i = 0; i < VECTOR_SIZE;++i)
{
	REGISTER_FILE[REG_DEST_ADDR + i] = RAM[SOURCE_ADDR + i + LOOP_COUNT0];
}
```



REG_DEST_ADDR(14 bits) determines start desination address for the load. REG_DEST_ADDR points to the register file.

SOURCE_ADDR(31 bits) determines start source address of the vector loading in RAM.

VECTOR_SIZE(14 bits) determines the size of the vector.

# LOAD1
Same as LOAD0 but uses LOOP_COUNT1.

Pseudo code
```c
unsigned int i;
for (i = 0; i < VECTOR_SIZE;++i)
{
	REGISTER_FILE[REG_DEST_ADDR + i] = RAM[SOURCE_ADDR + i + LOOP_COUNT1];
}
```

# STORE0
STORE0 instruction stores a vector from register file to RAM. Uses LOOP_COUNT0.

```c
STORE0 REG_SOURCE_ADDR, DEST_ADDR, VECTOR_SIZE
```
INSTRUCTION<63..59> = OPCODE

INSTRUCTION<58..45> = REG_SOURCE_ADDR

INSTRUCTION<44..14> = DEST_ADDR

INSTRUCTION<13..0> = VECTOR_SIZE


Pseudo code
```c
unsigned int i;
for (i = 0; i < VECTOR_SIZE;++i)
{
	RAM[DEST_ADDR + i + LOOP_COUNT0] = REGISTER_FILE[REG_SOURCE_ADDR + i];
}
```

REG_SOURCE_ADDR(14 bits) determines start source address for the store. REG_SOURCE_ADDR points to the register file.

DEST_ADDR(31 bits) determines start destination address of the vector loading in RAM.

VECTOR_SIZE(14 bits) determines the size of the vector.

# STORE1
Sames as STORE0 but uses LOOP_COUNT1.

Pseudo code
```c
unsigned int i;
for (i = 0; i < VECTOR_SIZE;++i)
{
	RAM[DEST_ADDR + i + LOOP_COUNT1] = REGISTER_FILE[REG_SOURCE_ADDR + i];
}
```


# WHILE

# FOR
Implements a for loop with a special loop counter. Uses loop counter of 14 bits unsigned.
```c
FOR LOOP_COUNTER_ADDR, START_VALUE, END_VALUE, INCREMENT, END_PC_ADDR
```
INSTRUCTION<63..59> = OPCODE

INSTRUCTION<58> = LOOP_COUNTER_ADDR

INSTRUCTION<57..44> = START_VALUE

INSTRUCTION<43..30> = END_VALUE

INSTRUCTION<29..16> = INCREMENT

INSTRUCTION<15..0> = CODE_LENGTH

Pseudo code
```c
for (LOOP_COUNTERS[LOOP_COUNTER_ADDR] = START_VALUE; LOOP_COUNTERS[LOOP_COUNTER_ADDR] < END_VALUE; LOOP_COUNTERS[LOOP_COUNTER_ADDR] += INCREMENT)
{
	//start code   START PC ADDRESS

	  ...

	//end code     END PC ADDRESS = START PC ADDRESS +  CODE_LENGTH
}
```

LOOP_COUNTER_ADDR (1 bit) chooses which loop counter to use.

START_VALUE (14 bits) sets the starting value of the loop counter.

END_VALUE (14 bits) sets the end value of the loop counter.

INCREMENT (14 bits) sets the loop counter increment value.

CODE_LENGTH (16 bits) sets the size of the code. The for loop's end PC values equals PC value of the for loop and the CODE_LENGTH



# DOT0
DOT0 instruction dot products the two operands and saves the resulting vector into the register file. Uses LOOP_COUNT0.

```c
DOT0 VECTOR_RESULT_ADDR, VECTOR_0_ADDR, VECTOR_1_ADDR, VECTOR_SIZE
```
INSTRUCTION<63..59> = OPCODE

INSTRUCTION<58..45> = VECTOR_RESULT_ADDR

INSTRUCTION<44..31> = VECTOR_0_ADDR

INSTRUCTION<30..17> = VECTOR_1_ADDR

INSTRUCTION<16..3> = VECTOR_SIZE

Pseudo code
```c
unsigned int i;
for (i = 0; i < VECTOR_SIZE;++i)
{
	REGISTER_FILE[VECTOR_RESULT_ADDR] += REGISTER_FILE[VECTOR_0_ADDR + i] * REGISTER_FILE[VECTOR_1_ADDR + i + LOOP_COUNT0];
}
```


VECTOR_RESULT_ADDR(14 bits) sets start desination address for the resulting vector.

VECTOR_0_ADDR(14 bits) sets start source address for operand 0.

VECTOR_1_ADDR(14 bits) sets start source address for operand 1.

VECTOR_SIZE(14 bits) sets the size of the vector.

# DOT1
Sames as DOT0 execpt but uses LOOP_COUNT1.


Pseudo code
```c
unsigned int i;
for (i = 0; i < VECTOR_SIZE;++i)
{
	REGISTER_FILE[VECTOR_RESULT_ADDR] += REGISTER_FILE[VECTOR_0_ADDR + i] * REGISTER_FILE[VECTOR_1_ADDR + i + LOOP_COUNT1];
}
```

# SUM0
SUM0 instruction does the summation of a vector
