import sys
from pathlib import Path
import os
import pandas
import re
import math
from enum import Enum

def findInstrSize(row):
	#Loop over to find the length of the opcode
	for i in range(5):
		if (row["LENGTH"+str(i)].tolist()[0] == 0):
			return i
	return 5


def intArrayToBytes(arr,size):

	formated_array = []
	# Loop over ints
	for num in arr:
		byte_arr = []
		# loop over bytes in int
		for i in range( int(size/8)):
			# shift by 8 bits
			shift = num >> (i*8)
			# get LSB
			single_byte = shift & 255
			# store big endian
			byte_arr.insert(0, single_byte)
		# extend
		formated_array.extend(byte_arr)

	return formated_array


def removeComments(line):
	# find position
	pos = line.find(";")

	# Check if valid
	if (pos != -1):
		# Cut string
		return line[0:pos]
	else:
		# return original
		return line


def tokenizeLine(line):
	# Split by commas
	line_split = re.split(' |,|\n|	',line)

	#Remove empty elements
	line_split = [x for x in line_split if x is not '']

	return line_split

def changeState(states,line,section_starts):
	#Get start of section
	pos = int(line[2], 0)

	#Get new state
	if (line[1] == ".text"):
		section_starts[0] = pos
		return (states.TEXT ,section_starts)
	elif  (line[1] == ".data"):
		section_starts[1] = pos
		return (states.DATA ,section_starts)
	else:
		return (states.NONE ,section_starts)




# Convert ASM to machine code
def ASMTOMC(asm_code,cpu_params,instr_table):

	# Get parameters
	INSTRUCTION_SIZE  =  cpu_params[ cpu_params["PARAMETER"] == "INSTRUCTION_SIZE"]["VALUE"].tolist()[0]
	OPCODE_SIZE = cpu_params[ cpu_params["PARAMETER"] == "OPCODE_SIZE"]["VALUE"].tolist()[0]

	#States
	states = Enum('states', 'NONE TEXT DATA')

	#Current state
	current_state = states.NONE

	# section starts  [TEXT,DATA]
	section_starts = [0,0]

	# text
	text_bit_list = bytearray()
	int_list = []

	# data
	data_bit_list = bytearray()

	# all
	all_bit_list = bytearray()

	# Parse each line
	for line in asm_code:

		#Finite state machine

		#NONE STATE
		if (current_state == states.NONE):

			# Tokenize
			line_split = tokenizeLine(line)

			# Check if line is empty
			if (len(line_split) == 0):
				continue

			# Check line is start of section
			if (line_split[0] == "section"):
				current_state, section_starts = changeState(states,line_split,section_starts)
				continue

		#Process TEXT
		elif (current_state == states.TEXT):

			#Remove comments
			line_split = removeComments(line)

			# Tokenize
			line_split = tokenizeLine(line_split)

			# Check if line is empty
			if (len(line_split) == 0):
				continue

			# Check line is start of section
			if (line_split[0] == "section"):
				current_state, section_starts = changeState(states,line_split,section_starts)
				continue

			#Get instruction data
			instr_data = instr_table[instr_table["INSTRUCTION"] == line_split[0]]

			# Check instruction for correct operands
			if len(line_split) < findInstrSize(instr_data):
				print("invalid operands")

			# convert string to bits
			bits = 0
			bit_pos = INSTRUCTION_SIZE
			for n in range(len(line_split)):
				if (n == 0):
					# get opcode in bits
					bit_pos -= OPCODE_SIZE
					bits = instr_data["OPCODE"].tolist()[0] << bit_pos
				else:
					# get operand in bits
					operand = int(line_split[n], 0)
					bit_length = (instr_data["LENGTH"+str(n-1)].tolist()[0])
					bit_pos -= bit_length
					if (operand  < math.pow(2, bit_length ) ):
						bits = bits |  ( operand  <<  bit_pos)
					else:
						print("invalid operands")


			#print ("{0:064b}".format(bits))

			int_list.append(  bits )

		#Proccess DATA
		elif (current_state == states.DATA):

			# Tokenize
			#line_split = tokenizeLine(line)

			# Check if line is empty
			#if (len(line_split) == 0):
				#continue

			# Check line is start of section
			#if (line_split[0] == "section"):
				#current_state, section_starts = changeState(states,line_split,section_starts)
				#continue

			#Append to array
			data_bit_list += bytearray(line.encode())

		else:
			print("invalid state")

	# 16bit Align

	# Convert INT to byte array
	text_bit_list  = bytearray( intArrayToBytes(int_list,64) )

	# PAD text
	all_bit_list += bytearray([0]*2*section_starts[0] )

	# ADD text
	all_bit_list += text_bit_list

	# PAD data
	all_bit_list += bytearray([0]*( (2*section_starts[1])  - len(all_bit_list)) )

	# ADD data
	all_bit_list += data_bit_list

	return all_bit_list

# Entry point
def main():
	# Settings
	instr_dir = "instructions.csv"
	cpu_params_dir = "cpu.csv"

	# Check arguments
	if len(sys.argv) < 3:
		# error
		print("python3 ./assembler.py [input file path]  [output file path]")

	else:
		# Get input file path
		input_file_path =  Path(str(sys.argv[1])).resolve()

		# Get output file path
		output_file_path =  Path(str(sys.argv[2])).resolve()

		# Check if file exists
		if Path(input_file_path).is_file():

			# Read CPU parameters
			cpu_params = pandas.read_csv(cpu_params_dir, delimiter=',', delim_whitespace=True)

			# Read instruction table
			instr_table = pandas.read_csv(instr_dir, delimiter=',', delim_whitespace=True)

			# Open asm file
			with open(input_file_path, 'r', errors='ignore') as in_file:

				# Read files in to list
				asm_code=in_file.readlines()

				print ("Parsing data")

				# Convert ASM to machine code
				machine_code = ASMTOMC(asm_code,cpu_params,instr_table)

				#Write file
				with open(output_file_path, 'wb') as out_file:
					print ("Writing data")
					out_file.write(machine_code)

		else:
			# error
			print("assembler: no file")

#0000000000000000010000000000000000000000000000001100000000000100
#0000000000000000100000000000000000000000000000001100000000000100
#0010110000000000011000000000100010000000000111000000000000001101

if __name__ == '__main__':
	main()
