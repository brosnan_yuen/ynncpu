# YNNCPU
Neural network FPGA accelerator designed in VHDL. Tested using the Nexys 4 DDR. Also includes an assembler.



# [CPU Architecture](./CPU/CPU.md)

![System Diagram](./docs/CPUsystem.png)




# CPU Features

Features of the CPU.
* 16 bit signed fixed point
* 64 bit instruction size
* Multiple instruction issue per cycle
* Out of order execution
* Neural network scheduling
* Multiple functional units
* Vector load/store
* Vector addition/subtraction
* Vector dot product
* Data compression in RAM
* 2^32 bytes of data access





# [Instruction Set Architecture](./assembler/instructions.md)

Specification for instruction set architecture.
* 16 bit alignment
* 16384 general registers
	* Each register has 16 bit signed fixed point number
	* Register number range in (-1.0,1.0)
	* Multiport register file
* 2 special loop counters (14 bits unsigned)
* Instruction cache size of 1024 bytes
* L1 cache size of 8192 bytes
* L2 cache mapped to RAM

[Instruction Parameters](./assembler/instructions.csv)

[CPU Parameters](./assembler/cpu.csv)


# Project Layout
* [asm_tests](./asm_tests/)       -- Unit tests for assembler
* [assembler](./assembler/)      -- Houses the assembler
	* [assembler.py](./assembler/assembler.py) -- Assembler script
	* [cpu.csv](./assembler/cpu.csv)      -- CPU parameters
	* [instructions.csv](./assembler/instructions.csv)     -- Instruction parameters
	* [instructions.md](./assembler/instructions.md)  -- Instruction Set Architecture
	* [README.md](./assembler/README.md)    -- Assembler help page
* [CPU](./CPU/)      -- CPU VHDL code
	* [cpu.vhd](./CPU/cpu.vhd)  -- Top level VHDL project
	* [cpu_states.vhd](./CPU/cpu_states.vhd)   -- CPU states for CPU FSM
	* [cpu_consts.vhd](./CPU/cpu_consts.vhd)   -- CPU constant generics
	* [control_unit.vhd](./CPU/cpu_consts.vhd)   -- CPU control unit for CPU FSM
	* [flash_controller.vhd](./CPU/cpu_consts.vhd)   -- Controls the CPU's flash interface
	* [control_unit.vhd](./CPU/cpu_consts.vhd)
	* [CPU.md](./CPU/CPU.md) -- CPU Architecture
* [generate](./generate/)   -- Scripts for generating VHDL and CPU files
	* [generate_cpu_generics.py](./generate/generate_cpu_generics.py)  --  Generate the [cpu_consts.vhd](./CPU/cpu_consts.vhd)
	* [random_vector_dot.py](./generate/random_vector_dot.py)    -- Generate assembly code for testing vector dot product
* [docs](./docs/)     -- CPU Documents and pictures
	* [CPUsystem.png](./docs/CPUsystem.png)-- Overall CPU Architecture
