
# CPU Architecture
![System Diagram](../docs/CPUsystem.png)

Items stored in [FLASH](./CPU.md#flash):
* Program Text
* Program Data


Items stored in [RAM](./CPU.md#ram):
* Program Text
* Program Data
* [L2 Cache](./CPU.md#l2-cache)
* Buffer

Core components of the CPU:
* [Control Unit](./CPU.md#control-unit)
* [Flash Controller](./CPU.md#flash-controller)
* [RAM Controller](./CPU.md#ram-controller)
* [Instruction Cache](./CPU.md#instruction-cache)
* [Instruction Decoder](./CPU.md#instruction-decoder)
* [Register File](./CPU.md#register-file)
* [Functional Units](./CPU.md#functional-units)
* [L1 Cache](./CPU.md#l1-cache)

# FLASH
FLASH holds the Program Text and the Program Data. The CPU's Memory Controller loads the required program text and program data from FLASH to [RAM](./CPU.md#ram). CPU only loads the data as needed by the instruction. CPU always loads the 0x00 memory location in FLASH.


Program Text contains the instructions to be executed by the CPU.

Program Data contains the data to be computed by the functional units.


# RAM

RAM holds part of the Program Text and the Program Data. Program Text and the Program Data is loaded as needed. RAM also has the [L2 Cache](./CPU.md#l2-cache) that contains previously used data. Buffer contains the read/write data from FLASH. At the start, RAM is loaded with memory location of 0x00 of FLASH. RAM's program text is loaded into the [Instruction Cache](./CPU.md#instruction-cache).   RAM's program data is loaded into the [Register File](./CPU.md#register-file).




# L2 Cache
L2 Cache contains the most used data that is not in the current program text and program data. L2 Cache is normally loaded into the [Register File](./CPU.md#register-file) and the [L1 Cache](./CPU.md#l1-cache). L2 Cache is far larger than the [L1 Cache](./CPU.md#l1-cache) but only has a single port.



# Control Unit

Control Unit controls all the other components in the CPU. Control Unit's FSM has the current state of the CPU. The current state determines the state of each individual component in the CPU. The individual components could be stopped or could be running.



# Flash Controller
The flash controller controls the read/writes to [FLASH](./CPU.md#flash) . The flash controller uses Quad SPI to interface with the on board flash. Flash controller has a buffer to read/writing as it can not real/write all the data in one cycle.



# RAM Controller
The RAM controller controls the read/writes to [RAM](./CPU.md#ram). The RAM controller uses 16 bit bus to interface with the RAM attached to the CPU. RAM controller has a buffer to read/writing as it can not real/write all the data in one cycle.



# Instruction Cache
Instruction cache stores the current instructions to be executed. The instruction cache contains multiple instructions and can provide multiple instructions to the [Control Unit](./CPU.md#control-unit) and the [Instruction Decoder](./CPU.md#instruction-decoder). This enables out of order execution.

# Instruction Decoder
Instruction decoder decodes the current instructions and schedules the executions of the instructions. Multiple instructions are issued in parallel. The scheduling is done to maximize the number of instructions currently executing. The scheduling also handles data hazards. Instruction decoder controls the [Register File](./CPU.md#register-file) and the [Functional Units](./CPU.md#functional-units).


# Register File
Register File contains the registers of the CPU. The register file has multiple ports. The multiple ports enables multiple concurrent read and writes. The register file has general registers and loop counter registers. The general registers are used to store data and perform operations. The loop counter registers contain the index of the current loops. The register file retrives data from the [RAM](./CPU.md#ram) or the [L1 Cache](./CPU.md#l1-cache).


# Functional Units
Functional Units are individual ALUs used to compute registers.
Functional units:
* Vector dot product
	* x1 16 bit multiplier
	* x1 16 accumulator
	* x3 14 bit counter
* Vector add/sub
	* x1 16 bit adder
	* x3 14 bit counter
* Vector summation
	* x1 16 bit accumulator
	* x3 14 bit counter


# L1 Cache
L1 cache contains most used data that is not in the register file. The L1 cache stores data for future usage in the register file.


# Start Sequence
1. Read FLASH memory location 0x00 into the [Flash Controller](./CPU.md#flash-controller)
2. Write the program text into the [Instruction Cache](./CPU.md#instruction-cache) . At the same time, write the program into the [RAM Controller](./CPU.md#ram-controller) and the [L1 Cache](./CPU.md#l1-cache)
3. Read the [Instruction Cache](./CPU.md#instruction-cache). At the same time, write the program into [RAM](./CPU.md#ram)
3. Write first few instructions into the [Control Unit](./CPU.md#control-unit) and the [Instruction Decoder](./CPU.md#instruction-decoder)
4. [Instruction Decoder](./CPU.md#instruction-decoder) decodes the instruction and [Control Unit](./CPU.md#control-unit) determines the next state


# Clocks
On board oscilator produces 100 MHz input clock.
100 MHz is feed into PLLs and produces:
* Control Unit - 100 MHz
* Flash controller - 50 MHz
* RAM controller - 300 MHz
* Instruction cache - 100 MHz
* Instruction decoder - 100 MHz
* Register File - 100 MHz
* Functional Units - 100 MHz
* L1 Cache - 100 MHz
