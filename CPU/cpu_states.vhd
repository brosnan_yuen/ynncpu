LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

package cpu_states is

-- Control Unit FSM
type CU_state_type is (CU_RESET, CU_LOAD_FLASH , CU_RUN, CU_RAM_STALL );

-- Flash controller FSM
type FC_state_type is ( FC_RESET, FC_LOAD );

-- RAM controller FSM
type RAM_state_type is ( RAM_RESET, RAM_LOAD , RAM_RUN );

-- Instruction cache FSM
type IC_state_type is ( IC_RESET, IC_LOAD, IC_RUN );

-- Instruction decode FSM
type ID_state_type is ( ID_RESET, ID_LOAD, ID_RUN );

-- Register file FSM
type RF_state_type is ( RF_RESET, RF_RUN );

-- Functional unit FSM
type func_state_type is ( FUNC_RESET, FUNC_RUN );

-- L1 cache FSM
type cache_state_type is ( CACHE_RESET, CACHE_LOAD, CACHE_RUN );


-- Flash controller feedback signal
type FC_feedback_type is ( FC_NORMAL , FC_LOAD_DONE );

-- Instruction decode feedback signal
type ID_feedback_type is ( ID_NORMAL , ID_RAM_STALL );


-- SPI for flash
type SPI_state_type is ( SPI_CLOCK_ALIGN, WREN_INSTR, WREN_END, WRR_INSTR ,WRR_DATA, WRR_END , DDRQIOR_INSTR );



end cpu_states;
