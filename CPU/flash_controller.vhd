LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.cpu_consts.all;
use work.cpu_states.all;

entity flash_controller is
	Port (
		-- CPU clock
		clk 			: in  STD_LOGIC;

		-- Flash controller state
		FC_state : in FC_state_type;

		--Flash controller feedback signal
		FC_feedback : out FC_feedback_type;


		-- Flash SPI chip select
		flash_CS 				: out  STD_LOGIC;

		-- Flash SPI clock
		flash_SCK 			: in  STD_LOGIC;

		-- Flash SPI IO0
		flash_IO0 			: inout  STD_LOGIC;

		-- Flash SPI IO1
		flash_IO1 			: inout  STD_LOGIC;

		-- Flash SPI IO2
		flash_IO2 			: inout  STD_LOGIC;

		-- Flash SPI IO3
		flash_IO3 			: inout  STD_LOGIC;




		-- Flash bus valid signal
		flash_data_valid 	: out  STD_LOGIC;

		-- Flash output data bus
		flash_data	: out std_logic_vector ((DATA_WIDTH-1) downto 0);

		-- Flash addr bus
		flash_addr	: out std_logic_vector ((FLASH_ADDR_SIZE-1) downto 0)

	);
end flash_controller;

architecture Behavioral of flash_controller is

--Clock align

-- SPI Timing Counter
signal SPI_counter : std_logic_vector ((SPI_COUNTER_SIZE-1) downto 0) :=  SPI_COUNTER_INIT;

-- SPI Address counter
signal SPI_addr_counter : std_logic_vector ((FLASH_ADDR_SIZE-1) downto 0) :=  FLASH_START_INSTR_ADDR;


-- SPI FSM
signal SPI_state : SPI_state_type := WREN_INSTR;
-- Clock Align
-- Write enable WREN
-- Write QUAD=1 in CR  using WRR
-- Send Read instruction DDRQIOR
-- Continous reading
-- Release CS

begin


process(clk,FC_state,SPI_counter,SPI_addr_counter,SPI_state,flash_SCK,flash_IO0,flash_IO1,flash_IO2,flash_IO3)
begin


	--SYNC code
	if (rising_edge(flash_SCK)) then
		--Wait for clock alignment
		if (SPI_state = SPI_CLOCK_ALIGN) and (FC_state = FC_LOAD) then
			--Set next state
			SPI_state <= WREN_INSTR;

			--Enable flash
			flash_CS <= '0';
		end if;

	end if;


	--SYNC code
	if (rising_edge(clk)) then

		--Flash controller FSM
		case FC_state is

			when FC_RESET => --RESET state
				--Reset counters
				SPI_counter <= SPI_COUNTER_INIT;
				SPI_addr_counter <=  FLASH_START_INSTR_ADDR;

				--Reset SPI state
				SPI_state <= SPI_CLOCK_ALIGN;

				--Give feedback
				FC_feedback <= FC_NORMAL;

				-- Disable flash
				flash_CS <= '1';

				-- flash data valid
				flash_data_valid <= '0';

				flash_IO0 <= '0';
				flash_IO1 <= '0';
				flash_IO2 <= '0';
				flash_IO3 <= '0';

			when FC_LOAD => --LOAD state

				--Process SPI
				case SPI_state is
					when SPI_CLOCK_ALIGN => -- Align two clocks
						--Wait for alignment

					when WREN_INSTR => --Send write enable instruction

						-- write instruction
						flash_IO0 <= SPI_WREN(to_integer(unsigned( SPI_counter(SPI_INSTR_ADDR_SIZE downto 1) )) );

						if (SPI_counter = SPI_INSTR_0) then
							-- Stop instruction
							SPI_state <= WREN_END;

							-- Disable flash
							flash_CS <= '1';
						end if;

						-- Increment
						SPI_counter <= std_logic_vector( unsigned( SPI_counter ) + (0 => '1', others => '0') ) ;

					when WREN_END =>

						if (SPI_counter = SPI_INSTR_1) then
							--New instruction
							SPI_state <= WRR_INSTR;

							--Reset counter
							SPI_counter <= SPI_COUNTER_INIT;

							--Enable flash
							flash_CS <= '0';
						else
							-- Count
							SPI_counter <= std_logic_vector( unsigned( SPI_counter ) + (0 => '1', others => '0') ) ;
						end if;

					when WRR_INSTR => --Write to register

						flash_IO0 <= SPI_WRR(to_integer(unsigned( SPI_counter(SPI_INSTR_ADDR_SIZE downto 1) )) );

						SPI_counter <= std_logic_vector( unsigned( SPI_counter ) + (0 => '1', others => '0') ) ;

						if (SPI_counter = SPI_INSTR_0) then
							-- Data
							SPI_state <= WRR_DATA;
						end if;

					when WRR_DATA => --Write CR[1] = 1

						if (SPI_counter = SPI_INSTR_2) then
							--Enable QUAD
							flash_IO0 <= '1';
						else
							flash_IO0 <= '0';
						end if;

						SPI_counter <= std_logic_vector( unsigned( SPI_counter ) + (0 => '1', others => '0') ) ;

						if (SPI_counter = SPI_INSTR_3) then
							-- Stop instruction
							SPI_state <= WRR_END;

							-- Disable flash
							flash_CS <= '1';
						end if;

					when WRR_END =>

						if (SPI_counter = SPI_INSTR_4) then
							--New instruction
							SPI_state <= DDRQIOR_INSTR;

							--Reset counter
							SPI_counter <= SPI_COUNTER_INIT;

							--Enable flash
							flash_CS <= '0';

							--Reset
							SPI_addr_counter <=  FLASH_START_INSTR_ADDR;
							flash_IO0 <= '0';
							flash_IO1 <= '0';
							flash_IO2 <= '0';
							flash_IO3 <= '0';
						else
							--Count
							SPI_counter <= std_logic_vector( unsigned( SPI_counter ) + (0 => '1', others => '0') ) ;
						end if;

					when DDRQIOR_INSTR =>

						-- write instruction
						flash_IO0 <= SPI_DDRQIOR(to_integer(unsigned( SPI_counter(SPI_INSTR_ADDR_SIZE downto 1) )) );

						if (SPI_counter = SPI_INSTR_0) then
							-- Stop instruction
							SPI_state <= WREN_END;

							-- Disable flash
							flash_CS <= '1';
						end if;

						-- Increment
						SPI_counter <= std_logic_vector( unsigned( SPI_counter ) + (0 => '1', others => '0') ) ;


					when others =>



				end case;




				if () then -- Completed flash loading
					FC_feedback <= FC_LOAD_DONE;
				end if;

			when others =>

		end case;

	end if;




end process;

end Behavioral;
