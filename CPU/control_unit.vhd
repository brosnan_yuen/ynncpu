LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.cpu_consts.all;
use work.cpu_states.all;

entity control_unit is
	Port (
		-- CPU clock
		clk 			: in  STD_LOGIC;

		-- CPU reset
		rst 			: in  STD_LOGIC;


		--Flash controller feedback signal
		FC_feedback : in FC_feedback_type;

		--Instruction decode  feedback signal
		ID_feedback : in ID_feedback_type;


		-- Flash controller state
		FC_state : out FC_state_type;

		-- RAM controller state
		RAM_state : out RAM_state_type;

		-- Instruction cache state
		IC_state : out IC_state_type;

		-- Instruction decode state
		ID_state : out ID_state_type;

		-- Register file state
		RF_state : out RF_state_type;

		-- Functional unit state
		func_state : out func_state_type;

		-- L1 cache state
		cache_state : out cache_state_type

	);
end control_unit;

architecture Behavioral of control_unit is

-- Control unit current state
signal cu_state : CU_state_type := CU_RESET;

begin


process(clk,rst,cu_state,FC_feedback,ID_feedback)
begin

	--SYNC code
	if (rising_edge(clk)) then

		if (rst = '1') then --RESET

			cu_state <= CU_RESET;

			FC_state <= FC_RESET;
			RAM_state <= RAM_RESET;
			IC_state <= IC_RESET;
			ID_state <= ID_RESET;
			RF_state <= RF_RESET;
			func_state <= FUNC_RESET;
			cache_state <= CACHE_RESET;

		else

			--Control unit FSM
			case cu_state is

				when CU_RESET => --RESET state
					--Next state
					cu_state <= CU_LOAD_FLASH;

					-- Outputs
					FC_state <= FC_LOAD;
					RAM_state <= RAM_LOAD;
					IC_state <= IC_LOAD;
					ID_state <= ID_LOAD;
					RF_state <= RF_RUN;
					func_state <= FUNC_RUN;
					cache_state <= CACHE_LOAD;

				when CU_LOAD_FLASH => --LOAD from flash state
					if (FC_feedback = FC_LOAD_DONE) then
						--Next state
						cu_state <= CU_RUN;

						-- Outputs
						FC_state <= FC_RESET;
						RAM_state <= RAM_RUN;
						IC_state <= IC_RUN;
						ID_state <= ID_RUN;
						RF_state <= RF_RUN;
						func_state <= FUNC_RUN;
						cache_state <= CACHE_RUN;
					end if;

				when CU_RUN => -- Normal run state
					if (ID_feedback = ID_RAM_STALL) then
						--Next state
						cu_state <= CU_RAM_STALL;

						-- Outputs
						FC_state <= FC_RESET;
						RAM_state <= RAM_RUN;
						IC_state <= IC_RUN;
						ID_state <= ID_RUN;
						RF_state <= RF_RUN;
						func_state <= FUNC_RUN;
						cache_state <= CACHE_RUN;
					end if;

				when CU_RAM_STALL =>


				when others =>


			end case;

		end if;

	end if;


end process;

end Behavioral;
