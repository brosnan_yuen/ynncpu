LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.cpu_consts.all;


entity fetch is
	Port (
		clk 			: in  STD_LOGIC;
		rst 			: in  STD_LOGIC;
		current_state 		: in  stage_state_type ;
		program_counter     : in  std_logic_vector ((DATA_WIDTH-1) downto 0);
		instruction_vector     : out std_logic_vector ( ((INSTRUCTION_SIZE*INSTRUCTION_SIZE) - 1) downto 0)
	);
end fetch;

architecture Behavioral of fetch is


	type ROM_TYPE is array (0 to 10 ) of std_logic_vector (15 downto 0);

	constant rom_content : ROM_TYPE := (
		0000 => "0100001001000000", --LOAD
	others => x"0000" ); -- NOP



begin


process(clk,rst,current_state,program_counter)
begin
	if (rising_edge(clk)) then
			if (rst = '1') then --RESET
					instruction_vector <= (others => '0');
			else

				case current_state is

					when RUN_STATE =>
							instruction <= rom_content(to_integer(unsigned(program_counter(7 downto 1))));

					when others =>
							instruction_vector <= (others => '0');

				end case;


			end if;
	end if;


end process;

end Behavioral;
