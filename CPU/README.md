# CPU
* [CPU](./CPU/)      -- CPU VHDL code
	* [cpu.vhd](./CPU/cpu.vhd)  -- Top level VHDL project
	* [cpu_states.vhd](./CPU/cpu_states.vhd)   -- CPU states for CPU FSM
	* [cpu_consts.vhd](./CPU/cpu_consts.vhd)   -- CPU constant generics
	* [control_unit.vhd](./CPU/cpu_consts.vhd)   -- CPU control unit for CPU FSM
	* [flash_controller.vhd](./CPU/cpu_consts.vhd)   -- Controls the CPU's flash interface
	* [control_unit.vhd](./CPU/cpu_consts.vhd)
	* [CPU.md](./CPU/CPU.md) -- CPU Architecture



# [CPU Architecture](./CPU/CPU.md)
