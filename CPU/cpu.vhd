LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.cpu_consts.all;
use work.cpu_states.all;

entity cpu is
	Port (
		-- Main clock for the CPU's blocks
		clk 			: in  STD_LOGIC;

		-- Reset signal
		rst 			: in  STD_LOGIC;




		-- Input data port to CPU
		in_port		: in  std_logic_vector ((IN_PORT_SIZE-1) downto 0);

		-- Output data port
		out_port	: out std_logic_vector ((OUT_PORT_SIZE-1) downto 0);




		-- Flash SPI chip select
		flash_CS 				: out  STD_LOGIC;

		-- Flash SPI clock
		flash_SCK 			: in  STD_LOGIC;

		-- Flash SPI IO0
		flash_IO0 			: inout  STD_LOGIC;

		-- Flash SPI IO1
		flash_IO1 			: inout  STD_LOGIC;

		-- Flash SPI IO2
		flash_IO2 			: inout  STD_LOGIC;

		-- Flash SPI IO3
		flash_IO3 			: inout  STD_LOGIC;





		-- DDR 2 Address
		ddr2_addr 			: out std_logic_vector ((DDR2_ADDR_SIZE-1) downto 0);

		--DDR 2 Bank select
		ddr2_ba 				: out std_logic_vector ((DDR2_BANK_SIZE-1) downto 0);

		-- Active-low Row Address Strobe
		ddr2_ras_n  			: out STD_LOGIC;

		--  Active-low Column Address Strobe
		ddr2_cas_n   			: out STD_LOGIC;

		--  Active-low Write Enable
		ddr2_we_n   			: out STD_LOGIC;

		-- DDR2 clock
		ddr2_clk  			: in STD_LOGIC;

		--  Active-high Memory Clock Enable
		ddr2_cke  			: out STD_LOGIC;

		--  Active-low Chip Select
		ddr2_cs_n  			: out STD_LOGIC;

		--  Output Data Mask
		ddr2_dm 				: out std_logic_vector ((DDR2_MASK_SIZE-1) downto 0);

		--  On-Die Termination
		ddr2_odt  			: out STD_LOGIC;

		--  Data input/output bus
		ddr2_dq 				: inout std_logic_vector ((DDR2_DATA_SIZE-1) downto 0);

		-- DDR2 Upper and lower strobe positive differential
		ddr2_dqs_p			: inout std_logic_vector ((DDR2_STROBE_SIZE-1) downto 0);

		-- DDR2 Upper and lower strobe negative differential
		ddr2_dqs_n			: inout std_logic_vector ((DDR2_STROBE_SIZE-1) downto 0)

	);
end cpu;

architecture Behavioral of cpu is

begin


process


begin



end process;

end Behavioral;
