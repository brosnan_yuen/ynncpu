import subprocess
from pathlib import Path
import os
import sys
import pandas

# Entry point
def main():

	print ("Generating CPU generics VHDL")

	# Head string for VHDL
	head = """

--Auto Generated by python file
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

package cpu_consts is

"""

	# Tail string for VHDL
	tail = """
end cpu_consts;
	"""

	#CPU parameters location
	cpu_params_dir = Path("../assembler/cpu.csv").resolve()

	#CPU address location
	cpu_addr_dir = Path("../assembler/cpu_addr.csv").resolve()

	#CPU instructions location
	instr_dir = Path("../assembler/instructions.csv").resolve()

	# CPU VHDL file
	cpu_vhdl_dir = Path("../CPU/cpu_consts.vhd").resolve()


	#LOAD CPU parameters CSV
	cpu_params = pandas.read_csv(cpu_params_dir, delimiter=',', delim_whitespace=True)

	#LOAD CPU address CSV
	cpu_addr = pandas.read_csv(cpu_addr_dir, delimiter=',', delim_whitespace=True)

	#LOAD CPU instructions CSV
	instr_table = pandas.read_csv(instr_dir, delimiter=',', delim_whitespace=True)


	# Get opcode size
	OPCODE_SIZE = cpu_params[ cpu_params["PARAMETER"] == "OPCODE_SIZE"]["VALUE"].tolist()[0]



	#Create middle text
	middle = ""

	# Generate VHDL code using CPU parameters
	middle += "\t" + "-- CPU parameters \n\n"
	for index, row in cpu_params.iterrows():
		#ADD VHDL comment
		middle += "\t" + "-- " + row['COMMENTS'] + "\n"
		#ADD VHDL constant integer
		middle += "\t" + "constant " + row['PARAMETER'] + " : integer := " + str(row['VALUE']) +  " ;" + "\n\n"
	middle += "\n\n"


	# Generate VHDL code using CPU addresses
	middle += "\t" + "-- CPU Addresses \n\n"
	for index, row in cpu_addr.iterrows():
		#ADD VHDL comment
		middle += "\t" + "-- " + row['COMMENTS'] + "\n"
		# Look up size
		DATA_SIZE = cpu_params[ cpu_params["PARAMETER"] == row['SIZE'] ]["VALUE"].tolist()[0]
		# ADD VHDL constant vector
		middle +=  "\t" + "constant " + row['PARAMETER'] + " : std_logic_vector (" + str(DATA_SIZE-1) + " downto 0) := \"" + format(row['VALUE'],   "0" + str(DATA_SIZE) + "b")   +  "\" ;" + "\n\n"
	middle += "\n\n"


	# Generate VHDL code using CPU Instructions
	middle += "\t" + "-- CPU Instructions \n\n"
	for index, row in instr_table.iterrows():
		#ADD VHDL comment
		middle +=  "\t" + "-- " + row['INSTRUCTION'] + " Instruction\n"
		# ADD VHDL constant vector
		middle +=  "\t" + "constant " + row['INSTRUCTION'] + "_INSTR : std_logic_vector (" + str(OPCODE_SIZE-1) + " downto 0) := \"" + format(row['OPCODE'],   "0" + str(OPCODE_SIZE) + "b")   +  "\" ;" + "\n\n"


	with open(cpu_vhdl_dir, 'w') as out_file:
		out_file.write(head+middle+tail)

	print ("Writing done")

if __name__ == '__main__':
	main()
