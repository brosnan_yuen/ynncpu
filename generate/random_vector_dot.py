import subprocess
from pathlib import Path
import os
import sys

# Entry point
def main():

	# Vector size
	VECTOR_SIZE = 200

	#Path to template
	template_path = "./random_vector_dot.asm"


	# Check arguments
	if len(sys.argv) < 2:
		# error
		print("python3 ./random_vector_dot.py [output file path]")

	#Get output dir
	output_file_path =  Path(str(sys.argv[1])).resolve()

	print("Generating assembly file for vector dot product")

	#Generate random array
	random_arr = bytearray(os.urandom(VECTOR_SIZE*2))

	#Read file
	with open(template_path, "rb") as binary_file:
		#Read in binary
		bt_arr = bytearray(binary_file.read())
		#Append random binary array
		bt_arr += random_arr

		#Write file
		with open(output_file_path, 'wb') as out_file:
			out_file.write(bt_arr)

	print("Asm file written")




if __name__ == '__main__':
	main()
