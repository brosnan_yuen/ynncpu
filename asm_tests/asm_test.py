import subprocess
import pathlib
import os
import binascii

import time

def CRC32_file(filename):
	buf = open(filename,'rb').read()
	buf = (binascii.crc32(buf) & 0xFFFFFFFF)
	return buf


# Entry point
def main():
	#tmp dir
	directory = "/tmp/asm/"

	#assembly file
	asm_files = ["working_test0.asm"]

	# binary files
	bin_files = ["/tmp/asm/working_test0.bin"]

	#CRC32 numbers
	CRC32_arr  = [2031269456]

	print("Running tests")

	#Create tmp dir
	pathlib.Path(directory).mkdir(parents=True, exist_ok=True)

	# CD assembler
	os.chdir(os.getcwd() + "/../assembler/" )

	# Check the output of each binary file by the assembler
	for l in range(len(bin_files)):
		# Run assembler
		subprocess.Popen(["python3", os.getcwd() + "/assembler.py",os.getcwd() + "/../asm_tests/"+asm_files[l],bin_files[l]])
		time.sleep(1)

		# Check CRC32
		if ((CRC32_file(bin_files[l])) == CRC32_arr[l]):
			print ("Passed: %s" % bin_files[l])
		else:
			print ("Failed: %s" % bin_files[l])




if __name__ == '__main__':
	main()
